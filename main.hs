
-- | Le type de donnée 'Vrai' a un habitant.
data Vrai = Vrai deriving Show
-- | Le type de donnée 'Faux' n'a pas d'habitant.
data Faux
-- | Règles de déduction
-- | ((P => Q) et P) => Q
modusPonens :: (p -> q, p) -> q
modusPonens (f, p) = f p
-- | Axiomes de l'implication
-- | P => (Q => P)
introductionImplication :: p -> (q -> p)
introductionImplication p q = p
-- | (P => (Q => R)) = > ((P => Q) => (P => R))
pseudoTransitivite :: (p -> (q -> r)) -> ((p -> q) -> (p -> r))
pseudoTransitivite impl1 impl2 p = impl1 p (impl2 p)

-- | (non P => non Q) => (Q => P)


-- | Axiomes de la conjonction 
-- | (P et Q) => P
eliminationConjonctionAGauche :: (p, q) -> p
eliminationConjonctionAGauche (p,_) = p
-- | (P et Q) => Q
-- IDEM a droite
-- | (R => P) => ((R => Q) => (R =>  (P et Q)))
introductionConjonction :: (r -> p) -> ((r -> q) -> (r -> (p,q)))
introductionConjonction f g r = (f r, g r)
-- | Axiomes de la disjonction
-- | P => (P ou Q)
introductionDisjonctionAGauche :: p -> Either p q
introductionDisjonctionAGauche p = Left p
-- | Q => (P ou Q)
introductionDisjonctionADroite :: q -> Either p q
introductionDisjonctionADroite q = Right q
-- | (P => R) => ((Q => R) => ((P ou Q) => R))
eliminationDisjonction :: (p -> r) -> ((q -> r) -> ((Either p q) -> r))
eliminationDisjonction f g (Left p) = f p
eliminationDisjonction f g (Right q) =  g q


-- | Premiers théorèmes
-- | Faux => Faux
fauxImpliqueFaux :: Faux -> Faux
fauxImpliqueFaux x = x
-- | Vrai => Vrai
vraiImpliqueVrai :: Vrai -> Vrai
vraiImpliqueVrai x = x
-- | Faux => Vrai
fauxImpliqueVrai :: Faux -> Vrai
fauxImpliqueVrai x = Vrai
-- | Vrai => Faux
-- vraiImpliqueFaux :: Vrai -> Faux
-- vraiImpliqueFaux x =  (PAS DE CONSTRUCTION POSSIBLE)
-- | P => Vrai
pImpliqueVrai :: p -> Vrai
pImpliqueVrai _ = Vrai
-- EXPLOSION
-- | Faux => P
-- explosion :: Faux -> p (Impossible de renvoyer à partir de faux un objet de n'importe quel type)
-- explosion x =

-- Tiers Exclu 
-- | (p ou non p)
-- te :: Either p (p -> Faux)
-- te = Right (const ) (Impossible de partir d'un type inconnu (en Left) et impossible de créer un objet de type faux )


-- Négations
-- | non (P et non P)
nonContradiction :: (p, (p -> Faux)) -> Faux
nonContradiction (p, f) = f p
-- | P => non non P
introductionDoubleNegation :: p -> (p -> Faux) -> Faux
introductionDoubleNegation p f = f p


-- | CURRYFICATION : ((a, b) -> c) -> a -> b -> c
-- Currification : fonction a deux arguments remplace par une fonction avec un seul argument 
-- qui renvoie à une fonction avec un seul argument
-- EXEMPLE de currification : (+4) 54   => 58j
-- | (P => (Q => r)) => (Q => (P => R))
-- | ((P et Q) => R) => (P => (Q => R))
curryfication :: ((p,q) -> r) -> p -> q -> r
curryfication f p q = f (p,q)
-- TRANSFORMER UNE FONCTION EN THEOREME 
-- RAISONEMENT PAR ABSURDE
-- | non non P => P
-- absurde :: ((p -> Faux)) -> Faux -> p ############# IMPOSSIBLE de définir p (car de tous type)
-- absurde f = 
-- | (P ou non P)
-- | (non P ou Q) => (P => Q)
-- | (non P => non Q) => (Q => P)

-- Loi de Morgan
-- | (non P et non Q) => non (P ou Q)
deMorgan1 :: (p -> Faux, q -> Faux) -> Either p q -> Faux
deMorgan1 (f,g) (Left p) = f p
deMorgan1 (f,g) (Right q) = g q

-- | non (P ou Q) => (non P et non Q)
deMorgan2 :: (Either p q -> Faux) -> (p -> Faux, q -> Faux)
deMorgan2 f = ((\p -> f (Left p)), (\q -> f (Right q)))

-- | (non P ou non Q) => non (P et Q)
deMorgan3 :: Either (p -> Faux) (q -> Faux) -> (p,q) -> Faux
deMorgan3 (Left f) (p,q) = f p
deMorgan3 (Right g) (p,q) = g q

-- | non (P et Q) => (non P ou non Q)
-- deMorgan4 :: ((p,q) -> Faux) -> (Either (p -> Faux) (q -> Faux))
-- deMorgan4 f =  (impossible )


-- Traduction de Gödel 

-- ABSURDE non non P => P
-- | non non non non P => non non P (Traduction de l'absurde en logique minimale)
godelAbsurde :: ((((p -> Faux) -> Faux) -> Faux) -> Faux) -> (p -> Faux) -> Faux
godelAbsurde f g = f (introductionDoubleNegation g)

-- TIERS EXCLU  (P ou non P)
-- | non non (non non P ou non non non P) (Traduction de godel du tiers exclu en logique minimale)
godelTiersExclu :: ((Either ((p -> Faux) -> Faux) (((p -> Faux) -> Faux) -> Faux)) -> Faux) -> Faux
godelTiersExclu f = nonContradiction (deMorgan2 f)

